import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatAutocompleteModule, MatOptionModule, MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { WeatherViewComponent } from './weather/components/weather-view/weather-view.component';
import { WeatherComponent } from './weather/containers/weather/weather.component';
import { WeatherSearchViewComponent } from './weather/components/weather-search-view/weather-search-view.component';
import { WeatherForecastViewComponent } from './weather/components/weather-forecast-view/weather-forecast-view.component';
import { SkyconsModule } from 'ngx-skycons';

@NgModule({
  declarations: [
    AppComponent,
    WeatherViewComponent,
    WeatherComponent,
    WeatherSearchViewComponent,
    WeatherForecastViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    SkyconsModule
  ],
  providers: [
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptor,
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
