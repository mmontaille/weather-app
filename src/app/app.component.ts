import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Meteou';

  constructor(private update: SwUpdate, private matSnackbar: MatSnackBar) {
    this.update.available.subscribe(
      () => {
        this.matSnackbar.open('New version available', 'Refresh', { duration: 10000 }).onAction().subscribe(
          () => {
            window.location.reload(true);
          }
        )
      }
    )
  }

}
