import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'weather-forecast-view',
  templateUrl: './weather-forecast-view.component.html',
  styleUrls: ['./weather-forecast-view.component.scss']
})
export class WeatherForecastViewComponent implements OnInit {

  @Input() forecast: any;

  constructor() { }

  ngOnInit() {
  }

  getSkycon(icon: string) {
    return icon.replace('-', '').toUpperCase();
  }

}
