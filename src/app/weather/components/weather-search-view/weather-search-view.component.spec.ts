import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherSearchViewComponent } from './weather-search-view.component';

describe('WeatherSearchViewComponent', () => {
  let component: WeatherSearchViewComponent;
  let fixture: ComponentFixture<WeatherSearchViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherSearchViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherSearchViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
