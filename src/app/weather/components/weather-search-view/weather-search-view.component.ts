import { Component, OnInit, ElementRef, ViewChild, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'weather-search-view',
  templateUrl: './weather-search-view.component.html',
  styleUrls: ['./weather-search-view.component.scss']
})
export class WeatherSearchViewComponent implements OnInit {

  @ViewChild('cityInput', { static: false }) cityInput: ElementRef;

  @Input() cities: any;

  @Output() inputChanged = new EventEmitter();
  @Output() citySelected = new EventEmitter();

  searchValue: string;
  expanded = false;

  constructor() { }

  ngOnInit() {
  }

  toggleExpand() {
    this.expanded = true;
    this.cityInput.nativeElement.focus();
  }

  inputChange() {
    this.inputChanged.emit(this.searchValue);
  }

  onOptionSelected(event: any) {
    this.expanded = false;
    this.searchValue = null;
    this.cityInput.nativeElement.blur();
    this.citySelected.emit(event);
  }

}
