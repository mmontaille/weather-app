import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'weather-view',
  templateUrl: './weather-view.component.html',
  styleUrls: ['./weather-view.component.scss']
})
export class WeatherViewComponent implements OnInit {

  @Input() weather: any;
  @Input() city: any;

  constructor() { }

  ngOnInit() { }

}
