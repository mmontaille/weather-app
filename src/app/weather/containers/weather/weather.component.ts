import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/core/services/weather.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as moment from 'moment';
import { City } from 'src/app/core/models/city';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  // Date
  today = new Date();
  tomorrow = new Date();
  startDate: any;
  timezone: any;

  // City
  currentCity: any;
  cities: any;

  // Weather
  weather: any;
  forecast: any;

  searchValue$ = new Subject();

  constructor(private _weather: WeatherService) { }

  ngOnInit() {
    this.tomorrow.setDate(this.today.getDate() + 1);
    this.tomorrow.setHours(0);
    this.currentCity = localStorage.getItem('currentCity') && this.isJson(localStorage.getItem('currentCity')) ?
      JSON.parse(localStorage.getItem('currentCity')) :
      new City('FR', 'Toulouse', 43.604259, 1.44367);
    this.getWeather(this.currentCity);
    this.searchValue$.pipe(
      debounceTime(300),
      distinctUntilChanged()
    ).subscribe((value: any) => {
      this.getCities(value);
    })
  }

  getWeather(city: any) {
    this.weather = null;
    this._weather.getWeatherForCity(city).subscribe(
      res => {
        this.weather = res;
        this.weather.currently.icon = this.weather.currently.icon.replace('-', '').toUpperCase();
        this.forecast = this.weather.hourly.data.filter(x => x.time > this.weather.currently.time).slice(0, 5);
        console.log(res)
        console.log(this.forecast)
      }
    )
  }

  getCities(search: string) {
    if (search !== null && search.length > 3) {
      this._weather.getJsonCities(search).subscribe(
        res => {
          this.cities = res;
        }
      )
    }
  }

  inputChanged(value: string) {
    this.searchValue$.next(value);
  }

  onOptionSelected(event: any) {
    this.currentCity = new City(event.option.value.country, event.option.value.name, event.option.value.coord.lat, event.option.value.coord.lon);
    localStorage.setItem('currentCity', JSON.stringify(this.currentCity));
    this.getWeather(this.currentCity);
    this.cities = null;
  }

  // Misc

  isJson(string) {
    try {
      JSON.parse(string);
      return true;
    } catch ($e) {
      return false;
    }
  }

}
