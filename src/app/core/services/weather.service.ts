import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { City } from '../models/city';

const API = 'https://api.darksky.net/forecast/ff74ea924504ab0c63df97bd7e4902bb';

const CORS = 'https://cors-anywhere.herokuapp.com/';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeatherForCity(city: City): Observable<any> {
    return this.http.get<any>(CORS + API + '/' + city.lat + ',' + city.lng + '?units=ca&exclude=daily');
  }

  getJsonCities(searchValue?: string) {
    return this.http.get('./assets/files/city.list.json').pipe(
      map(
        (cities: any[]) => {
          return cities.filter(city => city.name.toLowerCase().includes(searchValue.toLowerCase())).slice(0, 10);
        }
      )
    );
  }

}
